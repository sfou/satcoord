# pylint: skip-file
import pytest

from satcoord.coordinations.models import Coordination


@pytest.mark.django_db
def test_coord_create(coordination_factory, revision_factory):
    coord = coordination_factory.create()
    assert coord.status == Coordination.UNDER_REVIEW
    assert str(coord) == f"Coordination for mission: {coord.mission_name} - Under Review"

    assert coord.get_latest_revision() is None
    rev1 = revision_factory(coordination=coord)
    assert coord.get_latest_revision() == rev1
    rev2 = revision_factory(coordination=coord)
    assert coord.get_latest_revision() == rev2
    assert coord.get_absolute_url() == rev2.get_coordination_url()


@pytest.mark.django_db
def test_owner_and_perms(
    coordination_factory, revision_factory, radio_amateur_factory, sat_coord_panel_factory
):
    usr_owner, usr_not_owner = radio_amateur_factory.create_batch(2)
    panel_member = sat_coord_panel_factory.create()

    coord = coordination_factory.create(operator_responsible=usr_owner)
    revision_factory(coordination=coord)

    assert coord.is_owner(usr_owner)
    assert not coord.is_owner(usr_not_owner)
    assert not coord.is_owner(panel_member)

    assert coord.can_view_revisions(usr_owner)
    assert not coord.can_view_revisions(usr_not_owner)
    assert coord.can_view_revisions(panel_member)


@pytest.mark.django_db
def test_coord_review_revise(coordination_factory, revision_factory):
    coord1 = coordination_factory.create()
    rev1 = revision_factory(coordination=coord1)
    assert rev1.is_addressed == False
    assert rev1.comments == ""
    assert coord1.is_reviewable()
    coord1.get_detailed_status_display() == coord1.get_status_display()

    coord1.needs_changes("comment")
    latest_rev = coord1.get_latest_revision()
    assert latest_rev.id == coord1.id
    assert latest_rev.is_addressed
    assert latest_rev.comments == "comment"
    assert coord1.is_reviewable() == False
    assert coord1.get_detailed_status_display() == "Needs Changes"
    assert coord1.is_revisable()
    assert coord1.resolvement_date is None

    revision_factory(coordination=coord1)
    coord1.accept("comment2")
    assert coord1.status == Coordination.ACCEPTED
    assert coord1.resolvement_date
