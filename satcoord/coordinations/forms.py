from django import forms
from django.core.exceptions import ValidationError

from satcoord.coordinations.models import Beam, Coordination, Revision


class BootstrapFormMixin:  # pylint: disable=R0903
    """Mixin to add classes and 'required' to form fields"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs["class"] = "form-control"
            if hasattr(visible, "field") and visible.field.required:
                visible.field.widget.attrs["required"] = "required"


class CoordinationForm(BootstrapFormMixin, forms.ModelForm):
    """Form for creating a new Coordination application"""
    template_name = "coordinations/forms/new_coordination_form.html"

    class Meta:
        model = Coordination
        fields = ["mission_name", "supporting_organization", "description"]


class BeamForm(BootstrapFormMixin, forms.ModelForm):
    """Form for adding a Beam to a Coordination"""

    class Meta:
        model = Beam
        fields = [
            "direction",
            "modulation",
            "band",
            "service_area",
            "description",
        ]


class RevisionForm(forms.ModelForm):
    """Form for adding a Revision to a Coordination"""

    class Meta:
        model = Revision
        fields = ["file"]


class ReviewForm(BootstrapFormMixin, forms.Form):
    """Form for reviewing coordinations"""

    NEEDS_CHANGES = "c"
    ACCEPTED = "a"
    DECLINED = "d"
    REVIEW_STATUS_CHOICES = (
        (ACCEPTED, "Accepted"), (DECLINED, "Declined"), (NEEDS_CHANGES, "Needs Changes")
    )

    verdict = forms.ChoiceField(choices=REVIEW_STATUS_CHOICES)
    comments = forms.CharField(
        required=False, widget=forms.Textarea(attrs={
            "rows": 4,
            "style": "height:9rem"
        })
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial["verdict"] = self.NEEDS_CHANGES

    def clean(self):
        cleaned_data = super().clean()
        verdict = cleaned_data.get("verdict")
        comments = cleaned_data.get("comments")

        if verdict == self.NEEDS_CHANGES and not comments:
            raise ValidationError(
                "If the coordination application needs changes, \
the reviewer must provide relevant comments."
            )


class BeamDeleteForm(forms.Form):
    """A form for deleting beams of a coordination"""
    beams_to_delete = forms.ModelMultipleChoiceField(queryset=Beam.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop("queryset")
        super().__init__(*args, **kwargs)
        self.fields["beams_to_delete"].queryset = queryset
