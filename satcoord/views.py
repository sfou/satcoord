from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from satcoord.coordinations.models import Coordination, Revision


def robots(request):
    """Returns response for robots.txt requests"""
    data = render(request, "robots.txt", {"environment": settings.ENVIRONMENT})
    response = HttpResponse(data, content_type="text/plain; charset=utf-8")
    return response


class SettingsView(LoginRequiredMixin, TemplateView):
    """View responsible for rendering settings page"""
    template_name = "settings.html"


class HomeView(TemplateView):
    """View responsible for rendering home page"""
    template_name = "home.html"


class PanelMemberDashboardView(TemplateView):
    """View for displaying an overview for Panel Members"""

    template_name = "dashboard/panel_member_dashboard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        unaddressed_revisions = Revision.objects.filter(
            is_addressed=False
        ).select_related("coordination").order_by("-id")
        unaddressed_coords = []
        new_coords = []
        for rev in unaddressed_revisions:
            coord = rev.coordination
            coord.latest_revision = rev
            if rev.revision_number == 1:
                new_coords.append(coord)
            else:
                unaddressed_coords.append(coord)

        context["new_coords"] = new_coords
        context["has_new_coords"] = bool(new_coords)
        context["unaddressed_coords"] = unaddressed_coords
        context["has_unaddressed_coords"] = bool(unaddressed_coords)

        return context


class UserDashboardView(TemplateView):
    """View for displaying an overview for RadioAmateurs"""

    template_name = "dashboard/user_dashboard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        coords = Coordination.objects.filter(operator_responsible_id=self.request.user.id
                                             ).prefetch_related("revisions")
        need_change = []
        pending_review = []
        accepted = []
        declined = []
        for coord in coords:
            if coord.status == Coordination.UNDER_REVIEW:
                if coord.get_latest_revision().is_addressed:
                    need_change.append(coord)
                else:
                    pending_review.append(coord)
            elif coord.status == Coordination.ACCEPTED:
                accepted.append(coord)
            else:
                declined.append(coord)
        context["coords"] = coords
        context["need_change"] = need_change
        context["pending_review"] = pending_review
        context["accepted"] = accepted
        context["declined"] = declined
        context["has_coords"] = bool(coords)
        context["has_need_change"] = bool(need_change)
        context["has_pending_review"] = bool(pending_review)
        context["has_accepted"] = bool(accepted)
        context["has_declined"] = bool(declined)

        return context


@login_required
def get_dashboard_view(request, *args, **kwargs):
    """Selects appropriate dashboard view"""
    if request.user.groups.filter(name="Panel Members").exists():
        return PanelMemberDashboardView.as_view()(request, *args, **kwargs)
    return UserDashboardView.as_view()(request, *args, **kwargs)
