document.addEventListener("DOMContentLoaded", function() {
    let coordDescription = document.getElementById("coord-description");
    let coordExpander = document.getElementById("expander-container");
    if(coordDescription.scrollHeight > coordDescription.clientHeight) {
        coordExpander.classList.remove("d-none");
    }
});