from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.urls import include, path

from satcoord.users.views import RegisterView
from satcoord.views import HomeView, SettingsView, get_dashboard_view, robots

urlpatterns = [
    path("admin/", admin.site.urls),
    path("robots.txt", robots, name="robots"),
    path("", HomeView.as_view(), name="home"),
    path("coordinations/", include("satcoord.coordinations.urls")),
    path("accounts/register", RegisterView.as_view(), name="register"),
    path("accounts/login/", LoginView.as_view(redirect_authenticated_user=True), name="login"),
    path("accounts/", include("django.contrib.auth.urls")),
    path("overview/", get_dashboard_view, name="overview"),
    path("settings/", SettingsView.as_view(), name="settings")
]

if settings.DEBUG:
    urlpatterns += [
        path("__debug__/", include("debug_toolbar.urls")),
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
