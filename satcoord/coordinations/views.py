from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.db import transaction
from django.forms import inlineformset_factory
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotAllowed
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import exceptions as url_exceptions
from django.urls import reverse
from django.views.generic import DetailView, ListView

from satcoord.coordinations.forms import BeamDeleteForm, BeamForm, CoordinationForm, ReviewForm, \
    RevisionForm
from satcoord.coordinations.models import Beam, Coordination


class CoordinationListView(ListView):
    """View to display a list of coordinations"""
    queryset = Coordination.objects.order_by("status", "-id")
    template_name = "coordinations/coordinations_list.html"
    context_object_name = "coordinations"


class CoordinationDetailView(DetailView):
    """View to display a single coordination"""
    queryset = Coordination.objects.prefetch_related("beams"
                                                     ).select_related("operator_responsible")
    template_name = "coordinations/coordinations_detail.html"
    context_object_name = "coordination"

    def get_queryset(self):
        if not self.request.user.is_anonymous:
            return self.queryset.prefetch_related("revisions")
        return self.queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        coord = context["object"]
        context["show_revisions"] = coord.can_view_revisions(self.request.user)
        context["show_review_tools"] = bool(
            self.request.user.has_perm("review_coordinations") and coord.is_reviewable()
        )
        context["show_revise_tools"] = bool(coord.can_user_revise(self.request.user))
        context["review_form"] = ReviewForm()
        context["review_form_open"] = False
        back = self.request.GET.get("back")
        try:
            back_url = reverse(back or "coordination-list")
        except url_exceptions.NoReverseMatch:
            back_url = reverse("coordination-list")
        context["back_url"] = back_url
        if back:
            context["back"] = back

        return context


@login_required
def review_coordination_view(request, coord_id):
    """View for reviewing Coordinations"""

    if request.method == "OPTIONS":
        response = HttpResponse()
        response["allow"] = "POST"
        return response

    if request.method == "POST":
        coord = get_object_or_404(
            Coordination.objects.select_related("operator_responsible"
                                                ).prefetch_related("revisions", "beams"),
            id=coord_id
        )
        if not request.user.has_perm("review_coordinations"):
            return HttpResponseForbidden("You do not have permission to review coordinations")
        if not coord.is_reviewable():
            return HttpResponseForbidden("Coordination cannot be reviewed")

        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            verdict = review_form.cleaned_data["verdict"]
            comments = review_form.cleaned_data["comments"]

            if verdict == ReviewForm.ACCEPTED:
                coord.accept(comments)
            elif verdict == ReviewForm.DECLINED:
                coord.decline(comments)
            else:
                coord.needs_changes(comments)

            back = request.GET.get("back")

            return redirect(
                reverse("coordination-detail", kwargs={"pk": coord.id})
                + (f"?back={back}" if back else "")
            )

        return render(
            request,
            "coordinations/coordinations_detail.html", {
                "coordination": coord,
                "review_form": review_form,
                "show_revisions": True,
                "show_review_tools": True,
                "review_form_open": True
            },
            status=400
        )
    return HttpResponseNotAllowed(["POST"])


def create_beam_formset():
    """Helper method to create a Beam formset"""
    return inlineformset_factory(
        Coordination,
        Beam,
        form=BeamForm,
        fields=[
            "direction",
            "modulation",
            "band",
            "service_area",
            "description",
        ],
        can_delete=False,
        extra=1
    )


@login_required
def new_coordination_view(request):
    """View for creating a Coordination application"""
    if request.method == "OPTIONS":
        response = HttpResponse()
        response["allow"] = ",".join(["GET", "POST"])
        return response

    if request.method == "GET":
        coord_form = CoordinationForm()
        rev_form = RevisionForm()
        beam_formset = create_beam_formset()
        return render(
            request, "coordinations/coordinations_new.html", {
                "coord_form": coord_form,
                "rev_form": rev_form,
                "beam_formset": beam_formset,
                "beam_template_form": create_beam_formset()
            }
        )
    if request.method == "POST":
        coord_form = CoordinationForm(request.POST)
        rev_form = RevisionForm(request.POST, request.FILES)
        beam_formset = create_beam_formset()(request.POST)
        with transaction.atomic():
            if coord_form.is_valid():
                coord_instance = coord_form.save(commit=False)
                coord_instance.operator_responsible = request.user
                coord_instance.status = Coordination.UNDER_REVIEW
                coord_instance.save()
                if rev_form.is_valid():
                    rev_instance = rev_form.save(commit=False)
                    rev_instance.coordination = coord_instance
                    rev_instance.save()
                    if beam_formset.is_valid():
                        beams = beam_formset.save(commit=False)
                        for beam in beams:
                            beam.coordination = coord_instance
                            beam.save()
                        return redirect(
                            reverse("coordination-detail", kwargs={"pk": coord_instance.id})
                            + "?back=overview"
                        )
        return render(
            request,
            "coordinations/coordinations_new.html", {
                "coord_form": coord_form,
                "rev_form": rev_form,
                "beam_formset": beam_formset,
                "beam_template_form": create_beam_formset()
            },
            status=400
        )
    return HttpResponseNotAllowed(["GET", "POST"])


@login_required
def revise_coordination_view(request, coord_id):
    """View for creating a Coordination application"""
    coord = get_object_or_404(
        Coordination.objects.select_related("operator_responsible"
                                            ).prefetch_related("revisions", "beams"),
        id=coord_id
    )
    error_message = None
    if not coord.is_owner(request.user):
        error_message = "You do not have permission to revise this coordination"
    if not coord.is_revisable():
        error_message = "Coordination cannot be revised"
    if error_message:
        return HttpResponseForbidden(error_message)

    if request.method == "OPTIONS":
        response = HttpResponse()
        response["allow"] = ",".join(["GET", "POST"])
        return response

    if request.method == "GET":
        coord_form = CoordinationForm(instance=coord)
        rev_form = RevisionForm()
        beam_formset = inlineformset_factory(
            Coordination,
            Beam,
            form=BeamForm,
            fields=[
                "direction",
                "modulation",
                "band",
                "service_area",
                "description",
            ],
            can_delete=False,
            extra=0
        )(instance=coord)
        return render(
            request, "coordinations/coordinations_revise.html", {
                "coord": coord,
                "coord_form": coord_form,
                "rev_form": rev_form,
                "beam_formset": beam_formset,
                "beam_template_form": create_beam_formset(),
                "beam_delete_form": BeamDeleteForm(queryset=coord.beams)
            }
        )
    if request.method == "POST":
        coord_form = CoordinationForm(request.POST, instance=coord)
        rev_form = RevisionForm(request.POST, request.FILES)
        beam_formset = create_beam_formset()(request.POST, instance=coord)
        beam_delete_form = BeamDeleteForm(request.POST, queryset=coord.beams)
        try:
            with transaction.atomic():
                if coord_form.is_valid() and rev_form.is_valid() and beam_delete_form.is_valid(
                ) and beam_formset.is_valid():
                    coord_instance = coord_form.save()
                    rev_instance = rev_form.save(commit=False)
                    rev_instance.coordination = coord_instance
                    rev_instance.save()
                    beams_to_delete = beam_delete_form.cleaned_data["beams_to_delete"]
                    beams = beam_formset.save(commit=False)
                    for beam in beams:
                        if beam.id:
                            if beam.id not in beams_to_delete:
                                beam.save()
                        else:
                            beam.coordination = coord_instance
                            beam.save()
                    Beam.objects.filter(id__in=beams_to_delete).delete()

                    return redirect(
                        reverse("coordination-detail", kwargs={"pk": coord_instance.id})
                        + "?back=overview"
                    )
                raise ValidationError("Something went wrong, rollback required.")
        except ValidationError:
            return render(
                request,
                "coordinations/coordinations_revise.html", {
                    "coord": coord,
                    "coord_form": coord_form,
                    "rev_form": rev_form,
                    "beam_formset": beam_formset,
                    "beam_template_form": create_beam_formset(),
                    "beam_delete_form": beam_delete_form
                },
                status=400
            )
    return HttpResponseNotAllowed(["GET", "POST"])
