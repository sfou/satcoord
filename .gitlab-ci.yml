---
variables:
  GITLAB_CI_IMAGE_ALPINE: alpine:3.16.2
  GITLAB_CI_IMAGE_PYTHON: python:3.9.2
  GITLAB_CI_IMAGE_NODE: node:19.4
  GITLAB_CI_PYPI_TOX: tox~=3.27.0
  GITLAB_CI_IMAGE_DOCKER: 'docker:23.0.1'
  GITLAB_CI_PYPI_DOCKER_COMPOSE: 'docker-compose~=1.29.2'
  GITLAB_CI_POETRY_INSTALL: |-
    export POETRY_HOME=/opt/poetry
    python3 -m venv $POETRY_HOME
    $POETRY_HOME/bin/pip install poetry==1.3.2
    export PATH=$PATH:$POETRY_HOME/bin
stages:
  - static
  - build
  - test
  - deploy
# 'static' stage
sign_off:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache git
  script: 'git log --grep "^Signed-off-by: .\+<.\+\(@\| at \).\+\(\.\| dot \).\+>$" --invert-grep --format="Detected commit ''%h'' with missing or bad sign-off! Please read ''CONTRIBUTING.md''." --exit-code'
static_js_css:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_NODE}
  before_script:
    - yarn install --cache-folder .yarn --frozen-lockfile
  script:
    - yarn run css-lint
    - yarn run js-lint
    - yarn run build
  cache:
    - key:
        files:
          - yarn.lock
      paths:
          - .yarn
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - satcoord/static/css
      - satcoord/static/lib
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - node_modules/
      - .yarn
python_static:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_PYTHON}
  cache:
    - key:
        files:
          - poetry.lock
      paths:
          - .tox/pyX
  before_script:
    - |-
      export POETRY_HOME=/opt/poetry
      python3 -m venv $POETRY_HOME
      $POETRY_HOME/bin/pip install poetry==1.3.2
      export PATH=$PATH:$POETRY_HOME/bin
    - pip install ${GITLAB_CI_PYPI_TOX}
  script:
    - tox -e "flake8,isort,yapf,pylint"
# 'build' stage
build:
  stage: build
  needs:
    - job: sign_off
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - |-
      export POETRY_HOME=/opt/poetry
      python3 -m venv $POETRY_HOME
      $POETRY_HOME/bin/pip install poetry==1.3.2
      export PATH=$PATH:$POETRY_HOME/bin
    - poetry self add poetry-dynamic-versioning=0.20.0
    - pip install ${GITLAB_CI_PYPI_TOX}
  script:
    - rm -rf dist
    - tox -e build
# 'test' stage
test:
  stage: test
  needs: []
  image: ${GITLAB_CI_IMAGE_PYTHON}
  cache:
    - key:
        files:
          - poetry.lock
      paths:
          - .tox/pyX
  before_script:
    - |-
      export POETRY_HOME=/opt/poetry
      python3 -m venv $POETRY_HOME
      $POETRY_HOME/bin/pip install poetry==1.3.2
      export PATH=$PATH:$POETRY_HOME/bin
    - poetry self add poetry-dynamic-versioning=0.20.0
    - pip install ${GITLAB_CI_PYPI_TOX}
  script:
    - tox -e pytest
# 'deploy' stage
docker:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DOCKER}
  services:
    - ${GITLAB_CI_IMAGE_DOCKER}-dind
  before_script:
    - apk --update add py-pip
    - pip install "$GITLAB_CI_PYPI_DOCKER_COMPOSE"
  script:
    - |
      [ -z "$CI_REGISTRY_IMAGE" ] || {
          CACHE_IMAGE="$CI_REGISTRY_IMAGE/satcoord:$CI_COMMIT_REF_NAME"
          [ -z "$CI_COMMIT_TAG" ] || CACHE_IMAGE="$CI_REGISTRY_IMAGE/satcoord:latest"
          export CACHE_IMAGE
      }
    - docker-compose -f docker-compose.yml -f docker-compose.production-overrides.yml -f docker-compose.cache.yml pull cache_image || true
    - docker-compose -f docker-compose.yml -f docker-compose.production-overrides.yml -f docker-compose.cache.yml build --pull
    - |
      [ -z "$CI_REGISTRY_IMAGE" ] || {
          docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
          docker tag satcoord:latest $CI_REGISTRY_IMAGE/satcoord:$CI_COMMIT_REF_NAME
          docker push $CI_REGISTRY_IMAGE/satcoord:$CI_COMMIT_REF_NAME
          [ -z "$CI_COMMIT_TAG" ] || {
              docker tag satcoord:latest $CI_REGISTRY_IMAGE/satcoord:latest
              docker push $CI_REGISTRY_IMAGE/satcoord:latest
          }
      }
  only:
    refs:
      - main
      - tags
deploy:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install ${GITLAB_CI_PYPI_TOX}
  script:
    - rm -rf dist
    - tox -e "upload"
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD
  except:
    - triggers