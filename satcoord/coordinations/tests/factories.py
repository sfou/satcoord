import factory
from factory import fuzzy
from faker import Faker

from satcoord.coordinations.models import Beam, Coordination, Revision
from satcoord.users.tests.factories import RadioAmateurFactory

fake = Faker()
fake.seed_instance()
profile = fake.profile()

COORD_STATUSES = [stat[0] for stat in Coordination.COORD_STATUS_CHOICES]
BEAM_DIRECTIONS = [direction[0] for direction in Beam.DIRECTION_CHOICES]
BEAM_MODES = Beam.MODES
BEAM_BANDS = Beam.BANDS


class CoordinationFactory(factory.django.DjangoModelFactory):
    """Factory class for Coordination"""
    mission_name = factory.Sequence(lambda n: f"test_mission{n}")
    supporting_organization = profile["company"]
    description = fake.paragraph(nb_sentences=5)
    operator_responsible = factory.SubFactory(
        RadioAmateurFactory,
        callsign=factory.Sequence(lambda n: f"test_operator{n}", ),
        email=factory.LazyAttribute(lambda obj: f"{obj.callsign}@example.com")
    )

    class Meta:
        model = Coordination


class BeamFactory(factory.django.DjangoModelFactory):
    """Factory class for Beam"""

    coordination = factory.SubFactory(CoordinationFactory)
    direction = fuzzy.FuzzyChoice(BEAM_DIRECTIONS)
    modulation = fuzzy.FuzzyChoice(BEAM_MODES)
    band = fuzzy.FuzzyChoice(BEAM_BANDS)
    description = fake.paragraph(nb_sentences=5)
    service_area = fake.location_on_land()

    class Meta:
        model = Beam


class RevisionFactory(factory.django.DjangoModelFactory):
    """Factory class for Revision"""

    coordination = factory.SubFactory(CoordinationFactory)
    file = factory.django.FileField(filename="rev1.docx", data=b"test")

    class Meta:
        model = Revision
