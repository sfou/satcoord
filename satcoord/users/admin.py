from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .models import RadioAmateur, SatCoordPanel


class RadioAmateurAdmin(UserAdmin):
    """Admin class for RadioAmateur users"""
    fieldsets = (
        (None, {
            "fields": ("callsign", "email", "password")
        }),
        (_("Personal info"), {
            "fields": ("first_name", "last_name")
        }),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {
            "fields": ("last_login", "date_joined")
        }),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide", ),
                "fields": ("callsign", "email", "password1", "password2"),
            },
        ),
    )

    list_display = ("id", "callsign", "email", "first_name", "last_name", "is_staff")
    search_fields = ("callsign", "first_name", "last_name", "email")
    ordering = ("-id", )


class SatCoordPanelAdmin(RadioAmateurAdmin):
    """Admin class for Satcoord Panel Members"""

    def get_queryset(self, request):
        return SatCoordPanel.objects.all()


admin.site.register(RadioAmateur, RadioAmateurAdmin)
admin.site.register(SatCoordPanel, SatCoordPanelAdmin)
