def get_or_create_panel_members_group():
    """Creates a group for Panel Members and assigns permissions"""
    from django.contrib.auth.models import Group, Permission  # pylint: disable=C0415

    from satcoord.coordinations.models import Coordination  # pylint: disable=C0415

    panel_member_group, created = Group.objects.get_or_create(name="Panel Members")
    if created:
        perms_list = [perm_tuple[0] for perm_tuple in Coordination._meta.permissions]
        perms = Permission.objects.filter(codename__in=perms_list)
        for perm in perms:
            panel_member_group.permissions.add(perm)
    return panel_member_group
