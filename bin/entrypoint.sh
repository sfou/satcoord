#!/bin/sh -e
#
# SatCoord Django control script
#
# Copyright (C) 2018-2019 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

MANAGE_CMD="poetry run django-admin"
PROD_WSGI_SERVER_COMMAND="poetry run gunicorn"
DEV_WSGI_SERVER_COMMAND="poetry run ./manage.py runserver 0.0.0.0:8000"
DJANGO_APP="satcoord"

usage() {
	cat <<EOF
Usage: $(basename "$0") [OPTIONS]... [COMMAND]...
SatCoord Django control script.

COMMANDS:
  prepare               Collect static files and apply migrations
  run                   Run WSGI HTTP server
  develop               Run application in development mode

OPTIONS:
  --help                Print usage
EOF
	exit 1
}

prepare() {
	$MANAGE_CMD collectstatic --noinput --clear
	$MANAGE_CMD compress --force
	$MANAGE_CMD migrate --noinput
}

run() {
	prepare
    exec $PROD_WSGI_SERVER_COMMAND --bind 0.0.0.0:8000 "$DJANGO_APP".wsgi
}

develop() {
	prepare
	exec $DEV_WSGI_SERVER_COMMAND
}


parse_args() {
	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			initialize|develop|run)
				command="$arg"
				break
				;;
			*)
				usage
				exit 1
				;;
		esac
		shift
	done
}

main() {
	parse_args "$@"
	if [ -z "$command" ]; then
		usage
		exit 1
	fi
	if [ -x "manage.py" ]; then
		MANAGE_CMD="poetry run ./manage.py"
	fi
	"$command"
}

main "$@"
