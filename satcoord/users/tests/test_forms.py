# pylint: skip-file

import pytest
from faker import Faker

from satcoord.users.forms import RadioAmateurCreationForm
from satcoord.users.models import RadioAmateur


@pytest.mark.django_db
def test_user_form(radio_amateur_factory):
    usr = radio_amateur_factory.build()
    password = Faker().password()
    form_data = {
        "callsign": usr.callsign,
        "email": usr.email,
        "first_name": usr.first_name,
        "last_name": usr.last_name,
        "password1": password,
        "password2": password,
    }
    form = RadioAmateurCreationForm(data=form_data)
    assert form.is_valid()
    form.save()
    assert RadioAmateur.objects.all().count() == 1
