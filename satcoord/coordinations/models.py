"""
The models for the coordination process
"""

from django.conf import settings
from django.db import models, transaction
from django.urls import reverse
from django.utils.timezone import now


class Coordination(models.Model):
    """An application for IARU frequency coordination"""
    UNDER_REVIEW = "r"
    ACCEPTED = "a"
    DECLINED = "d"
    COORD_STATUS_CHOICES = (
        (UNDER_REVIEW, "Under Review"), (ACCEPTED, "Accepted"), (DECLINED, "Declined")
    )

    date = models.DateTimeField(default=now)
    resolvement_date = models.DateTimeField(null=True, blank=True)
    mission_name = models.CharField(unique=True, max_length=50)
    operator_responsible = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL
    )
    supporting_organization = models.CharField(max_length=127)
    description = models.TextField()
    status = models.CharField(max_length=1, choices=COORD_STATUS_CHOICES, default=UNDER_REVIEW)

    def __str__(self):
        return f"Coordination for mission: {self.mission_name} - {self.get_status_display()}"

    def __review_coord(self, comments, new_status=None):
        """Helper method for setting status"""
        latest_rev = self.get_latest_revision()
        with transaction.atomic():
            if new_status:
                self.status = new_status
                self.resolvement_date = now()
                self.save()
            latest_rev.is_addressed = True
            latest_rev.comments = comments
            latest_rev.save()

    def accept(self, comments=""):
        """Mark coordination as accepted"""
        self.__review_coord(comments, new_status=self.ACCEPTED)

    def decline(self, comments=""):
        """Mark coordination as declined"""
        self.__review_coord(comments, new_status=self.DECLINED)

    def needs_changes(self, comments):
        """Adds comments to latest revision"""
        self.__review_coord(comments)

    def is_owner(self, user):
        """Return True if user is the one who issued the coordination application"""
        return not user.is_anonymous and user.id == self.operator_responsible_id

    def can_view_revisions(self, user):
        """Return True if user is allowed to view revisions"""
        return bool(self.is_owner(user) or user.has_perm("view_revisions"))

    def get_absolute_url(self):
        """Returns the absolute url for the instance detail view"""
        return reverse("coordination-detail", kwargs={"pk": self.pk})

    def get_latest_revision(self):
        """Returns the latest revision of this coordination"""
        try:
            return self.revisions.latest("id")
        except Revision.DoesNotExist:
            return None

    def is_reviewable(self):
        """Returns whether a panel member can review the coordination"""
        if self.status == Coordination.UNDER_REVIEW:
            latest_rev = self.get_latest_revision()
            if latest_rev and not latest_rev.is_addressed:
                return True
        return False

    def is_revisable(self):
        """Returns whether the applicant can provide revisions for the coordination"""
        if self.status == Coordination.UNDER_REVIEW:
            latest_rev = self.get_latest_revision()
            if latest_rev and latest_rev.is_addressed:
                return True
        return False

    def can_user_revise(self, user):
        """Returns whether user can provide revisions for the coordination"""
        return self.is_owner(user) and self.is_revisable()

    def get_detailed_status_display(self):
        """Returns a detailed status"""
        if self.status == Coordination.UNDER_REVIEW:
            latest_rev = self.get_latest_revision()
            if latest_rev and latest_rev.is_addressed:
                return "Needs Changes"
        return self.get_status_display()

    class Meta:
        permissions = [
            ("view_revisions", "Can view the coordination's revisions"),
            ("review_coordinations", "Can review a coordination"),
        ]


class Beam(models.Model):
    """A beam that corresponds to a Coordination"""
    E2S = "e2s"
    S2E = "s2e"
    S2S = "s2s"
    DIRECTION_CHOICES = ((E2S, "Earth to space"), (S2E, "Space to earth"), (S2S, "Space to space"))
    MODES = (
        "AFSK",
        "AFSK S-Net",
        "AFSK SALSAT",
        "AHRPT",
        "AM",
        "APT",
        "BPSK",
        "BPSK PMT-A3",
        "CERTO",
        "CW",
        "DQPSK",
        "DSTAR",
        "DUV",
        "FFSK",
        "FM",
        "FMN",
        "FSK",
        "FSK AX.100 Mode 5",
        "FSK AX.100 Mode 6",
        "FSK AX.25 G3RUH",
        "GFSK",
        "GFSK Rktr",
        "GFSK/BPSK",
        "GMSK",
        "HRPT",
        "LRPT",
        "LSB",
        "MFSK",
        "MSK",
        "MSK AX.100 Mode 5",
        "MSK AX.100 Mode 6",
        "OFDM",
        "OQPSK",
        "PSK",
        "PSK31",
        "PSK63",
        "QPSK",
        "QPSK31",
        "QPSK63",
        "SSTV",
        "USB",
        "WSJT",
    )

    BANDS = ("HF", "UHF", "VHF", "SHF", "EHF")

    coordination = models.ForeignKey(Coordination, related_name="beams", on_delete=models.CASCADE)
    direction = models.CharField(max_length=3, choices=DIRECTION_CHOICES)
    band = models.CharField(max_length=127, choices=zip(BANDS, BANDS))
    modulation = models.CharField(max_length=127, choices=zip(MODES, MODES))
    description = models.TextField(blank=True)
    service_area = models.CharField(max_length=128)

    def __str__(self):
        return f"{self.get_direction_display()} beam for {self.coordination}"


class Revision(models.Model):
    """A revision of a coordinatiion"""
    file = models.FileField(upload_to='revisions/%Y/%m/%d/')
    comments = models.TextField(blank=True)
    coordination = models.ForeignKey(
        Coordination, on_delete=models.CASCADE, related_name="revisions"
    )
    date = models.DateTimeField(default=now)
    revision_number = models.PositiveSmallIntegerField()
    is_addressed = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.revision_number = Revision.objects.filter(coordination_id=self.coordination_id
                                                           ).count() + 1
        super().save()

    def get_coordination_url(self):
        """Returns the url of the coordination"""
        return self.coordination.get_absolute_url()

    def __str__(self):
        return f"Revision #{self.revision_number} for {self.coordination}"
