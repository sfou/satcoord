document.addEventListener("DOMContentLoaded", function() { 
    let beamForms = document.querySelectorAll(".beam-form");
    let beamFormsContainer = document.querySelector("#beam-forms-container");
    let addBeamButton = document.querySelector("#add-beam-button");
    let totalBeamForms = document.querySelector("#id_beams-TOTAL_FORMS");
    let currentFormNum = beamForms.length;
    let removeButtons = document.querySelectorAll(".remove-beam-form-button");
    let deleteButtons = document.querySelectorAll(".delete-beam-form-button");
    let restoreButtons = document.querySelectorAll(".restore-beam-form-button");
    let formRegex = /beams-(\d){1}/g;
    let deleteSelect = document.getElementById("id_beams_to_delete");
    // eslint-disable-next-line
    let beamTemplateFormHTML = beamTemplateForm; // Variable is declared in the html
    addBeamButton.addEventListener("click", addBeamForm);
    removeButtons.forEach(btn => btn.addEventListener("click", removeBeamForm));
    deleteButtons.forEach(btn => btn.addEventListener("click", markBeamAsDeleted));
    restoreButtons.forEach(btn => btn.addEventListener("click", restoreBeam));

    // Reset the hidden select for deleting beams in the revise form
    for (let option of deleteSelect.options) {
        option.selected = false;
    }

    // Helper function to create element from HTML string
    function createElementFromHTML(htmlString) {
        var div = document.createElement("div");
        div.innerHTML = htmlString.trim();
        return div.firstChild;
    }

    // Click handler for delete beam buttons in revise form
    function markBeamAsDeleted(e) {
        e.preventDefault();
        let delButton = e.currentTarget;
        let form;
        form = document.getElementById(delButton.dataset.parentForm);
        let overlay = form.querySelector(".crossed-out-overlay");
        overlay.classList.remove("d-none");
        overlay.classList.add("d-block");
        overlay.classList.add("show");
        delButton.classList.remove("d-block");
        delButton.classList.add("d-none");
        let restoreButton = form.querySelector(".restore-beam-form-button");
        restoreButton.classList.remove("d-none");
        restoreButton.classList.add("d-block");
        
        let delBeamId = delButton.dataset.beamId;
        for (let option of deleteSelect.options) {
            if(option.value==delBeamId) {
                option.selected = true;
            }
        }
    }

    // Click handler for restore beam buttons in revise form
    function restoreBeam(e) {
        e.preventDefault();
        let restoreButton = e.currentTarget;
        let form;
        form = document.getElementById(restoreButton.dataset.parentForm);
        let overlay = form.querySelector(".crossed-out-overlay");
        overlay.classList.remove("show");
        overlay.classList.remove("d-block");
        overlay.classList.add("d-none");
        restoreButton.classList.remove("d-block");
        restoreButton.classList.add("d-none");
        let deleteButton = form.querySelector(".delete-beam-form-button");
        deleteButton.classList.add("d-block");
        deleteButton.classList.remove("d-none");

        let restoreBeamId = restoreButton.dataset.beamId;
        for (let option of deleteSelect.options) {
            if(option.value==restoreBeamId) {
                option.selected = false;
            }
        }
    }

    // Click handler for remove beam buttons in new coord & revise forms
    function removeBeamForm(e) {
        e.preventDefault();
        let form;
        form = document.getElementById(e.currentTarget.dataset.parentForm);
        let formIdx = form.dataset.idx;
        form.classList.remove("show");
        setTimeout(() => form.remove(), 100);

        currentFormNum--;
        totalBeamForms.setAttribute("value", `${currentFormNum}`);
        decrementFormNumBelowIdx(formIdx);
    }

    function decrementFormNumBelowIdx(deletedFormIdx) {
        let beamForms = document.querySelectorAll(".beam-form");

        beamForms.forEach(form => {
            if (form.dataset.idx > deletedFormIdx) {
                let old_idx = Number(form.dataset.idx);
                form.setAttribute("id", `form-beams-${old_idx - 1}`);
                form.querySelector("legend").innerHTML = `#${old_idx}`;
                form.innerHTML = form.innerHTML.replace(formRegex, `beams-${old_idx - 1}`);
                form.dataset.idx = old_idx - 1;
                form.querySelector(".remove-beam-form-button").addEventListener("click", removeBeamForm);
            }
        });
    }

    // Click handler for add beam buttons in new coord & revise forms
    function addBeamForm(e) {
        e.preventDefault();
        let newForm = createElementFromHTML(beamTemplateFormHTML);
        currentFormNum++;
        newForm.innerHTML = newForm.innerHTML.replace(formRegex, `beams-${currentFormNum-1}`);
        newForm.querySelector(".remove-beam-form-button").addEventListener("click", removeBeamForm);
        newForm.querySelectorAll(".field-errors").forEach(errorDiv => errorDiv.remove());
        newForm.querySelectorAll("input").forEach(input => input.setAttribute("value", ""));
        newForm.querySelectorAll("textarea").forEach(textarea => textarea.innerHTML = "");
        newForm.querySelectorAll("select > option").forEach(option => option.removeAttribute("selected"));
        newForm.setAttribute("id", `form-beams-${currentFormNum-1}`);
        newForm.dataset.idx = currentFormNum-1;
        newForm.classList.remove("show");
        let newFormLegend = newForm.querySelector("legend");
        newFormLegend.innerHTML = `#${currentFormNum}`;
        totalBeamForms.setAttribute("value", `${currentFormNum}`);
        newForm = beamFormsContainer.insertBefore(newForm, beamForms[-1]);
        setTimeout(() => newForm.classList.add("show"), 100);
    }
});